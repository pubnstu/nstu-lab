#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>

#define N 100
#define _countof(array) (sizeof(array) / sizeof(array[0]))

int isrussian(char symbol)
{
	char alphabet[33] = {'а',
						 'б',
						 'в',
						 'г',
						 'д',
						 'е',
						 'ё',
						 'ж',
						 'з',
						 'и',
						 'й',
						 'к',
						 'л',
						 'м',
						 'н',
						 'о',
						 'п',
						 'р',
						 'с',
						 'т',
						 'у',
						 'ф',
						 'х',
						 'ц',
						 'ч',
						 'щ',
						 'ъ',
						 'ь',
						 'ы',
						 'э',
						 'у',
						 'я',
						 'ю'};

	for (size_t i = 0; i < strlen(alphabet); i++)
		if (alphabet[i] == symbol)
			return 1;

	return 0;
}

void removeDuplicateOfLastLetter(char source[N], char result[N])
{
	char letter;
	int counter, letter_pos, length;

	length = strlen(source);

	if (length < 2)
		return;

	for (size_t i = length - 1; i > 0; i--)
		if (isalpha(source[i]) | isrussian(source[i]))
		{
			letter = source[i];
			letter_pos = i;
			break;
		}

	if (!letter)
		return;

	counter = 0;

	for (size_t i = 0; i < length; i++)
		if ((source[i] != letter) || (letter_pos == i))
			result[i - counter] = source[i];
		else
			counter += 1;

	result[length - counter] = '\0';
}

int main1()
{
	char str[N], edited_str[N];

	setlocale(LC_ALL, "Russian");

	printf_s("Введите строку (максимум символов: %d): ", N);
	fgets(str, N, stdin);

	removeDuplicateOfLastLetter(str, edited_str);

	printf_s("Результат: %s", edited_str);

	return 0;
}