#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>
#include <math.h>

int Count_Pos(int number)
{
    int p = 1;
    while ((number / (int)pow(10, p)) > 0)
        p++; // цикл с предусловием
    return p;
}

char *getStepsOfTwo(int k)
{
    char *dest;

    int step = 1;
    int degree = 1;
    int str_length = 0;
    int num_length = 0;
    int temp_num;

    dest = (char *)malloc(sizeof(char) * k);

    while (1)
    {
        num_length = Count_Pos(step);
        temp_num = step;

        if (str_length + num_length > k)
        {
            for (size_t i = str_length + num_length - k; i > 0; i--)
            {
                dest[str_length] = (temp_num / (int)pow(10, i) + '0');
                temp_num -= temp_num / (int)pow(10, i - 1);
                str_length += 1;
            }

            break;
        }

        for (size_t i = num_length; i > 0; i--)
        {
            dest[str_length] = (temp_num / (int)pow(10, i - 1) + '0');
            temp_num -= temp_num / (int)pow(10, i - 1) * (int)pow(10, i - 1);
            str_length += 1;
        }

        degree++;

        if (degree > 31)
            break;

        step *= 2;
    }
    dest[str_length] = '\0';

    return dest;
}

int main()
{
    int k = 0;
    char *str;

    printf_s("Введите k (максимум: 2^31): ");
    scanf_s("%d", &k);

    str = getStepsOfTwo(k);

    printf_s("%s", str);

    return 0;
}