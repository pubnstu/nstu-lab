#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>

char *getString(int &length)
{
    char *string;
    int scan; // поменять название

    while (true)
    {
        printf_s("Введите длину строки: ");
        scan = scanf_s("%d", &length);

        if (length > 0 && scan == 1)
            break;

        printf_s("Вы ввели неверные данные. Попробуйте еще раз.\n");
        while (getchar() != '\n')
            ; // чистка буфера
    }

    length++;

    string = new char[length];
    while (getchar() != '\n')
        ; // чистка буфера

    printf_s("Введите строку: ");
    fgets(string, length, stdin);

    string[length - 1] = ' ';

    return string;
}

char *removeDuplicateOfLastLetter(char *word, int count)
{
    char *edited_word, letter;
    int count_of_changes = 0;
    int pos = 0;

    for (size_t i = count; i != 0; i--)
        if (isalpha(word[i]))
        {
            letter = word[i];
            pos = i;
            break;
        }

    if (!letter)
        return word;

    for (size_t i = 0; i < count; i++)
    {
        if (word[i] == letter && pos != i)
            count_of_changes++;
    }

    edited_word = (char *)malloc(sizeof(char) * count - count_of_changes);
    count_of_changes = 0;

    for (size_t i = 0; i < count; i++)
    {
        if (word[i] != letter || (word[i] == letter && pos == i))
            edited_word[i - count_of_changes] = word[i];
        else
            count_of_changes++;
    }

    edited_word[count - count_of_changes] = ' ';

    return edited_word;
}

char *getWordFromString(char *string, int starting_pos, int length)
{
    char *word;

    word = (char *)malloc(sizeof(char) * length);

    for (size_t i = starting_pos; i < starting_pos + length; i++)
        word[i - starting_pos] = string[i];

    return word;
}

char *searchWordsAndEdit(char *ptrIn, int length)
{
    int from_pos = 0;
    int count = 0;
    int i = 0;
    char *word, *new_string, *edited_word;

    char *string = ptrIn;

    new_string = (char *)malloc(sizeof(char) * length);
    new_string[0] = '\0';

    for (size_t i = 0; i < length; i++)
    {
        if (string[i] == ' ' || string[i] == '\n')
        {
            word = getWordFromString(string, from_pos, from_pos + count);
            edited_word = removeDuplicateOfLastLetter(word, count - from_pos);

            from_pos += count + 1;
            strcat(new_string, edited_word);
        }

        else
            count++;
    }

    return new_string;
}

int main()
{
    char *ptrIn, *ptrOut;
    int length;

    setlocale(LC_ALL, "Russian");
    ptrIn = getString(length);
    ptrOut = searchWordsAndEdit(ptrIn, length);

    printf_s("%s", ptrOut);

    return 0;
}